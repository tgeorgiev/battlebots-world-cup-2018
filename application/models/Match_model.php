<?php

class Match_model extends CI_Model {

    protected $table = 'matchgame';

    public $id;

    public function get($id)
    {
        $query = $this->db->get_where($this->table, array('id' => $id));
        return $query->row();
    }

    public function get_all()
    {
        $query = $this->db->get($this->table);
        return $query->result();
    }

    public function update($id)
    {
        $data = array(
            'points1' => $this->input->post('points1'),
            'points2' => $this->input->post('points2'),
            'finished' => $this->input->post('finished')
        );

        $this->db->update($this->table, $data, array('id' => $id));
    }

    public function prepare_nextMatch($matchId)
    {
        $match = $this->db->get_where($this->table, array('id' => $matchId))->row();
        $nextStage = $match->stage-1;

        // there should be a winner
        $winnerTeamId = ($match->points2 > $match->points1) ? $match->team2_id : $match->team1_id;

        $where = "tournament_id={$match->tournament_id} AND stage={$nextStage} AND (team1_id IS NULL OR team2_id IS NULL)";
        $this->db->where($where);
        $nextMatch = $this->db->get($this->table)->row();

        if(!$nextMatch->team1_id) {
            $this->db->update($this->table, array('team1_id' => $winnerTeamId), array('id' => $nextMatch->id));
        } else {
            $this->db->update($this->table, array('team2_id' => $winnerTeamId), array('id' => $nextMatch->id));
        }
    }
}