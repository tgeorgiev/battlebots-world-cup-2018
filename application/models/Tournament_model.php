<?php

class Tournament_model extends CI_Model {

    protected $table = 'tournament';

    public $id;
    public $name;

    public function get($id)
    {
        $query = $this->db->get_where($this->table, array('id' => $id));
        return $query->row();
    }

    public function get_all()
    {
        $query = $this->db->get($this->table);
        return $query->result();
    }

    public function get_all_withTeams()
    {
        $this->db->select('tournament.id, tournament.name, team.name as team_name');
        $this->db->from($this->table);
        $this->db->join('tournament_team', 'tournament.id = tournament_team.tournament_id');
        $this->db->join('team', 'tournament_team.team_id = team.id');
        $query = $this->db->get();
        return $query->result();
    }

    public function getTeams($tournamentId)
    {
        $this->db->select('team.id, team.name');
        $this->db->from('tournament_team');
        $this->db->join('team', 'tournament_team.team_id = team.id');
        $this->db->where('tournament_team.tournament_id', $tournamentId);
        $query = $this->db->get();
        return $query->result();
    }

    public function getTournamentStages($tournamentId)
    {
        $this->db->select_max('stage');
        $query = $this->db->get_where('matchgame', array('tournament_id' => $tournamentId));

        return $query->row();
    }

    public function get_matches($tournamentId)
    {
        $query = $this->db->get_where('matchgame', array('tournament_id' => $tournamentId));
        return $query->result();
    }

    public function add()
    {
        $teamsArr = $this->input->post('teams[]');
        $tournamentData = array(
            'name' => $this->input->post('name')
        );
        $this->db->trans_start();
        $this->db->insert($this->table, $tournamentData);
        $tournamentID = $this->db->insert_id();

        $tournamentTeamData = array();
        if(is_array($teamsArr) && !empty($teamsArr)) {
            foreach ($teamsArr as $team) {
                $tournamentTeamData[] = array('tournament_id' => $tournamentID, 'team_id' => (int)$team);
            }
            $this->db->insert_batch('tournament_team', $tournamentTeamData);
            $this->newTournamentCreateScheme($tournamentID);
        }

        $this->db->trans_complete();
    }

    public function edit($id)
    {
        $data = array(
            'name' => $this->input->post('name')
        );

        $this->db->update($this->table, $data, array('id' => $id));
    }

    public function delete($id)
    {
        $this->db->delete($this->table, array('id' => $id));
    }

    public function newTournamentCreateScheme($tournamentID)
    {
        $this->load->model('tournament_model');
        $tournamentTeams = $this->tournament_model->getTeams($tournamentID);

        $stages = intval( log(count($tournamentTeams)) / log(2) );

        // known matches
        $matchTeams = array_chunk($tournamentTeams, 2);
        foreach ($matchTeams as $team) {
            $matchArr[] = array(
                'team1_id'  => (int)$team[0]->id,
                'team2_id'  => (int)$team[1]->id,
                'stage'     => $stages,
                'tournament_id' => $tournamentID
            );
        }

        // rest of the planned matches
        $restMatchArr = array();
        for ($i = $stages-2; $i>=0; $i--) {
            $matches = array_fill(0, pow(2,$i), array('stage' => $i+1, 'tournament_id' => $tournamentID));
            $restMatchArr = array_merge($restMatchArr,$matches);
        }

        $this->db->trans_start();
        $num = $this->db->insert_batch('matchgame', $matchArr);
        $num2 = $this->db->insert_batch('matchgame', $restMatchArr);
        $this->db->trans_complete();
        return $num+$num2;
    }

}