<?php

class Team_model extends CI_Model {

    protected $table = 'team';

    public $id;
    public $name;

    public function get($id)
    {
        $query = $this->db->get_where($this->table, array('id' => $id));
        return $query->row();
    }

    public function get_all()
    {
        $query = $this->db->get($this->table);
        return $query->result();
    }

    public function get_last_ten_entries()
    {
        $query = $this->db->get($this->table, 10);
        return $query->result();
    }

    public function add()
    {
        $data = array(
            'name' => $this->input->post('name')
        );

        $this->db->insert($this->table, $data);
    }

    public function edit($id)
    {
        $data = array(
            'name' => $this->input->post('name')
        );

        $this->db->update($this->table, $data, array('id' => $id));
    }

    public function delete($id)
    {
        $this->db->delete($this->table, array('id' => $id));
    }

}