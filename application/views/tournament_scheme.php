<?php require_once 'partials/header.php'; ?>

<main role="main" class="container">
	<div class="my-3 p-3 bg-white rounded shadow-sm">
		<h6 class="border-bottom border-gray pb-2 mb-2">Tournament Scheme <small><?php echo $tournament->name ?></small></h6>
		<table class="table table-striped table-sm">
			<thead>
			<tr>
				<th scope="col">Stage</th>
				<th scope="col">Team A</th>
				<th scope="col">VS</th>
				<th scope="col">Team B</th>
				<th scope="col">Result</th>
				<th scope="col">Finished</th>
				<th scope="col">Match ID</th>
				<th scope="col">actions</th>
			</tr>
			</thead>
			<tbody>
			<?php foreach($matches as $match) : ?>
			<tr>
				<th scope="row"><?php echo $match->stage; ?></th>
				<td><?php echo isset($teamsArr[$match->team1_id]) ? $teamsArr[$match->team1_id] : ''; ?></td>
				<td>vs</td>
				<td><?php echo isset($teamsArr[$match->team2_id]) ? $teamsArr[$match->team2_id] : ''; ?></td>
				<td><?php echo $match->points1 . ' : ' . $match->points2; ?></td>
				<td><?php echo $match->finished ? 'yes' : 'no'; ?></td>
				<td><?php echo $match->id; ?></td>
				<td>
					<?php if (!$match->finished && $match->team1_id && $match->team2_id) : ?>
						<a href="<?php echo base_url("match/update/$match->id"); ?>" class="btn btn-sm btn-outline-primary">enter results</a>
					<?php endif; ?>
				</td>
			</tr>
			<?php endforeach; ?>
			</tbody>
		</table>
	</div>
</main>

<?php require_once 'partials/footer.php';