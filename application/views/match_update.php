<?php require_once 'partials/header.php'; ?>

<main role="main" class="container">
	<div class="my-3 p-3 bg-white rounded shadow-sm">
		<h6 class="border-bottom border-gray pb-2 mb-2">Update Match Results</h6>

		<?php echo validation_errors('<div class="alert alert-danger">', '</div>'); ?>

		<?php echo form_open("match/update/$match->id"); ?>
		<div class="form-group">
			<div class="input-group">
				<div class="input-group-prepend">
					<span class="input-group-text">Points <?php echo $team1->name . ' vs ' . $team2->name; ?></span>
				</div>
				<?php echo form_input('points1', $match->points1, array('class'=>'form-control')); ?>
				<?php echo form_input('points2', $match->points2, array('class'=>'form-control')); ?>
			</div>
		</div>
		<div class="form-group">
			<div class="input-group">
				<div class="input-group-prepend">
					<div class="input-group-text">
						<?php echo form_checkbox('finished', 1, false); ?>
					</div>
				</div>
				<label for="finished" class="form-control">Match has finished</label>
			</div>
		</div>
		<button onclick="goBack()" class="btn btn-default">Cancel</button>
		<input type="submit" name="submit" value="Save" class="btn btn-primary float-right" />

		</form>
	</div>
</main>

<?php require_once 'partials/footer.php';