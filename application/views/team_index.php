<?php require_once 'partials/header.php'; ?>

<main role="main" class="container">
	<div class="my-3 p-3 bg-white rounded shadow-sm">

		<h6 class="border-bottom border-gray pb-2 mb-2">Teams</h6>
		<a href="<?php echo base_url("team/add/"); ?>" class="btn btn-sm btn-primary float-right mb-2 mt-2">New Team</a>
		<table class="table table-striped table-sm">
			<thead>
			<tr>
				<th scope="col">ID</th>
				<th scope="col">name</th>
				<th scope="col" style="width: 15%">actions</th>
			</tr>
			</thead>
			<tbody>
			<?php foreach($teams as $team) : ?>
			<tr>
				<th scope="row"><?php echo $team->id; ?></th>
				<td><?php echo $team->name; ?></td>
				<td>
					<a href="<?php echo base_url("team/edit/$team->id"); ?>" class="btn btn-sm btn-outline-secondary">edit</a>
					<a href="<?php echo base_url("team/delete/$team->id"); ?>" class="btn btn-sm btn-outline-danger">delete</a>
				</td>
			</tr>
			<?php endforeach; ?>
			</tbody>
		</table>
	</div>
</main>

<?php require_once 'partials/footer.php';