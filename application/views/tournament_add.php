<?php require_once 'partials/header.php'; ?>

<main role="main" class="container">
	<div class="my-3 p-3 bg-white rounded shadow-sm">
		<h6 class="border-bottom border-gray pb-2 mb-2"><?php echo $title; ?></h6>

		<?php echo validation_errors('<div class="alert alert-danger">', '</div>'); ?>

		<?php echo form_open(); ?>
		<div class="form-group">
			<?php
			echo form_label('Name', 'name');
			echo form_input('name',isset($_POST['name'])?$_POST['name']:'', array('class' => 'form-control'));

			$options = array(
				'8'		=> '8',
				'16'	=> '16',
				'32'	=> '32',
				'64'	=> '64',
			);
			echo form_label('Number of teams', 'num_teams');
			echo form_dropdown('num_teams', $options, '8', array('class' => 'form-control', 'id' => 'num-teams'));
			?>
		</div>
		<div class="form-group" id="team-selection">
			<label for="teams[]">Select Teams</label>
			<div class="row">
			<?php
				$teamChunks = array_chunk($teams,8);
				foreach ($teamChunks as $teamChunk) {
					echo '<div class="col">';
					foreach ($teamChunk as $team) {
						echo '<div class="form-check">';
						echo form_checkbox('teams[]', $team->id, false, array('class' => 'form-check-input'));
						echo form_label($team->name, 'teams[]', array('class' => 'form-check-label'));
						echo '</div>';
					}
					echo '</div>';
				}
			?>
			</div>
		</div>


		<a href="/tournament" class="btn btn-secondary" role="button">Cancel</a>
		<input type="submit" name="submit" value="Save" class="btn btn-primary float-right" />

		</form>

	</div>
</main>

<?php require_once 'partials/footer_scripts.php'; ?>
<script>
	$(function(){
		var $checkboxes = $('#team-selection input[type="checkbox"]');

		$checkboxes.change(function(){
			var countCheckedCheckboxes = $checkboxes.filter(':checked').length;
			if (countCheckedCheckboxes > $('#num-teams').val()) {
				alert('Select maximum '+$('#num-teams').val()+' teams');
			}
		});
	});
</script>

<?php require_once 'partials/footer.php';