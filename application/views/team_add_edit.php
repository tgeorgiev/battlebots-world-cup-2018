<?php require_once 'partials/header.php'; ?>

<main role="main" class="container">
	<div class="my-3 p-3 bg-white rounded shadow-sm">
		<h6 class="border-bottom border-gray pb-2 mb-2"><?php echo $title; ?></h6>

		<?php echo validation_errors('<div class="alert alert-danger">', '</div>'); ?>

		<?php echo form_open(); ?>
		<div class="form-group">
			<label for="name">Name</label>
			<input type="input" name="name" value="<?php echo isset($team->name) ? set_value('name', $team->name) : ''; ?>" class="form-control" />
		</div>

		<a href="/team" class="btn btn-secondary" role="button">Cancel</a>
		<input type="submit" name="submit" value="Save" class="btn btn-primary float-right" />

		</form>
	</div>
</main>

<?php require_once 'partials/footer.php';