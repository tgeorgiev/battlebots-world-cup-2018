<?php require_once 'partials/header.php'; ?>

<main role="main" class="container">
	<div class="my-3 p-3 bg-white rounded shadow-sm">
		<h6 class="border-bottom border-gray pb-2 mb-2">Tournaments</h6>
		<a href="<?php echo base_url("tournament/add/"); ?>" class="btn btn-sm btn-primary float-right mb-2 mt-2">New Tournament</a>
		<table class="table table-striped table-sm">
			<thead>
			<tr>
				<th scope="col">ID</th>
				<th scope="col" style="width: 20%">name</th>
				<th scope="col">teams</th>
				<th scope="col" style="width: 15%">actions</th>
			</tr>
			</thead>
			<tbody>
			<?php foreach($tournaments as $tournament) : ?>
			<tr>
				<th scope="row"><?php echo $tournament->id; ?></th>
				<td><a href="<?php echo base_url("tournament/scheme/$tournament->id"); ?>"><?php echo $tournament->name; ?></a></td>
				<td><?php echo isset($tournamentTeams[$tournament->id]) ? implode(', ', $tournamentTeams[$tournament->id]) : ''; ?></td>
				<td>
					<a href="<?php echo base_url("tournament/edit/$tournament->id"); ?>" class="btn btn-sm btn-outline-secondary">edit</a>
					<a href="<?php echo base_url("tournament/delete/$tournament->id"); ?>" class="btn btn-sm btn-outline-danger">delete</a>
				</td>
			</tr>
			<?php endforeach; ?>
			</tbody>
		</table>
	</div>
</main>

<?php require_once 'partials/footer.php';