<?php
defined('BASEPATH') || exit('No direct script access allowed');

/*
 * Fixtures is used to fill the DB with data. Call it from http://bwc.local/fixtures
 */
class Fixtures extends CI_Controller {

    function index() {
        if(ENVIRONMENT != 'development')
            return;

        $this->db->query("SET FOREIGN_KEY_CHECKS = 0");

        $this->_team();
        $this->_tournament();
        $this->_match();

        $this->db->query("SET FOREIGN_KEY_CHECKS = 1");
    }

    protected function _showResult($table,$num) {
        printf("%s: %d rows affected<br>\n", $table, $num);
    }

    function _team() {
        $table = 'team';

        $teamArr = include APPPATH.'fixtures/team.php';

        $this->db->truncate($table);
        $num = $this->db->insert_batch($table, $teamArr);
        $this->_showResult($table,$num);
    }

    function _tournament() {
        $table = 'tournament';

        $teamArr = include APPPATH.'fixtures/team.php';
        $tournamentArr = include APPPATH.'fixtures/tournament.php';

        $this->db->truncate($table);
        $num = $this->db->insert_batch($table, $tournamentArr);
        $this->_showResult($table,$num);

        // fill tournament_team table
        $data = array();
        shuffle($teamArr); // tournament 1
        $teamIDs = array_column($teamArr, 'id');
        $data = array_merge($data, $this->_oneToManyArray(1, $teamIDs, array('tournament_id','team_id')));

        shuffle($teamArr); // tournament 2
        $teamIDs = array_column($teamArr, 'id');
        $data = array_merge($data, $this->_oneToManyArray(2, $teamIDs, array('tournament_id','team_id')));

        shuffle($teamArr); // tournament 3
        $teamArr = array_slice($teamArr, 0, 8);
        $teamIDs = array_column($teamArr, 'id');
        $data = array_merge($data, $this->_oneToManyArray(3, $teamIDs, array('tournament_id','team_id')));

        $this->db->truncate('tournament_team');
        $num = $this->db->insert_batch('tournament_team', $data);
        $this->_showResult('tournament_team',$num);
    }

    function _match() {
        $this->db->truncate('matchgame');
        $this->load->model('tournament_model');
        $affectedRows = 0;

        $tournaments = $this->tournament_model->get_all();
        foreach ($tournaments as $tournament) {
            $affectedRows += $this->tournament_model->newTournamentCreateScheme($tournament->id);
        }

        $this->_showResult('matchgame',$affectedRows);
    }

    protected function _oneToManyArray($id, $manyArr, $keys) {
        $data = array();
        foreach($manyArr as $el) {
            $data[] = array($keys[0] => $id, $keys[1] => $el);
        }

        return $data;
    }
}