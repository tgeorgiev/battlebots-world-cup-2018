<?php

defined('BASEPATH') || exit('No direct script access allowed');

class Team extends CI_Controller {

    public function index()
    {
        $this->load->model('team_model');
        $data = array(
            'teams' => $this->team_model->get_all()
        );
        $this->load->view('team_index', $data);
    }

    public function add_edit($id = 0)
    {
        $this->load->helper('form');
        $this->load->library('form_validation');
        $this->load->model('team_model');

        $this->form_validation->set_rules('name', 'Name', 'required|max_length[140]|is_unique[team.name]');

        $data = array(
            'title' => $id ? 'Edit Team' : 'Add Team',
            'team' => $this->team_model->get($id)
        );

        if ($this->form_validation->run() === FALSE)
        {
            $this->load->view('team_add_edit', $data);

        }
        else
        {
            if(!empty($id)) {
                $this->team_model->edit($id);
            } else {
                $this->team_model->add();
            }

            redirect('/team');
        }
    }

    public function delete($id)
    {
        $this->load->model('team_model');

        $this->team_model->delete($id);
        redirect('/team');
    }
}