<?php

defined('BASEPATH') || exit('No direct script access allowed');

class Match extends CI_Controller {

    public function index()
    {
        $this->load->model('match_model');

        $data = array(
            'matches'       => $this->match_model->get_all()
        );
        $this->load->view('match_index', $data);
    }

    public function update($id)
    {
        $this->load->helper('form');
        $this->load->library('form_validation');
        $this->load->model('match_model');
        $this->load->model('team_model');

        $this->form_validation->set_rules('points1', 'Points1', 'required|greater_than_equal_to[0]');
        $this->form_validation->set_rules('points2', 'Points2', 'required|greater_than_equal_to[0]|differs[points1]');

        $match = $this->match_model->get($id);
        $data = array(
            'match' => $match,
            'team1' => $this->team_model->get($match->team1_id),
            'team2' => $this->team_model->get($match->team2_id)
        );

        if ($this->form_validation->run() === FALSE)
        {
            $this->load->view('match_update', $data);
        }
        else
        {
            $this->match_model->update($id);

            // if match is finished prepare next match for the winner
            if($this->input->post('finished')) {
                $this->match_model->prepare_nextMatch($id);
            }

            redirect('/tournament/scheme/'.$match->tournament_id);
        }
    }
}