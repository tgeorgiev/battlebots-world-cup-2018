<?php

defined('BASEPATH') || exit('No direct script access allowed');

class Tournament extends CI_Controller {

    public function index()
    {
        $this->load->model('tournament_model');
        $tournaments = $this->tournament_model->get_all_withTeams();
        $tournamentTeams = array();
        foreach ($tournaments as $val) {
            $tournamentTeams[$val->id][] = $val->team_name;
        }
        $data = array(
            'tournaments'       => $this->tournament_model->get_all(),
            'tournamentTeams'   => $tournamentTeams
        );
        $this->load->view('tournament_index', $data);
    }

    public function scheme($tournamentId)
    {
        $this->load->model('tournament_model');
        $this->load->model('team_model');
        $teams = $this->team_model->get_all();
        $teamsArr = array();
        foreach ($teams as $team) {
            $teamsArr[$team->id] = $team->name;
        }

        $matches = $this->tournament_model->get_matches($tournamentId);
        $data = array(
            'matches'       => $matches,
            'tournament'    => $this->tournament_model->get($tournamentId),
            'stages'        => $this->tournament_model->getTournamentStages($tournamentId)->stage,
            'teamsArr'      => $teamsArr
        );
        $this->load->view('tournament_scheme', $data);
    }

    public function add()
    {
        $this->load->helper('form');
        $this->load->library('form_validation');
        $this->load->model(array('tournament_model','team_model'));

        $this->form_validation->set_rules('name', 'Name', 'required|max_length[140]|is_unique[tournament.name]');
        $this->form_validation->set_rules('num_teams', 'Number of Teams', 'required|in_list[8,16,32,64]');
        $this->form_validation->set_rules('teams[]', 'Teams', 'required|callback_rule_valid_team_selection');

        $data = array(
            'title' => 'Add Tournament',
            'teams' => $this->team_model->get_all()
        );

        if ($this->form_validation->run() === FALSE)
        {
            $this->load->view('tournament_add', $data);
        }
        else
        {
            $tournamentID = $this->tournament_model->add();
            redirect('/tournament');
        }
    }

    public function edit($id)
    {
        $this->load->helper('form');
        $this->load->library('form_validation');
        $this->load->model(array('tournament_model'));

        $this->form_validation->set_rules('name', 'Name', 'required|max_length[140]|is_unique[tournament.name]');

        $data = array(
            'title' => 'Edit Tournament',
            'tournament' => $this->tournament_model->get($id)
        );

        if ($this->form_validation->run() === FALSE)
        {
            $this->load->view('tournament_edit', $data);
        }
        else
        {
            $this->tournament_model->edit($id);
            redirect('/tournament');
        }
    }

    public function delete($id)
    {
        $this->load->model('tournament_model');

        $this->tournament_model->delete($id);
        redirect('/tournament');
    }

    public function rule_valid_team_selection()
    {
        $teams = $this->input->post('teams[]');
        if(empty($teams) || (int)$this->input->post('num_teams') != count($teams)) {
            $this->form_validation->set_message('rule_valid_team_selection', 'The {field} selected should be equal to "Number of teams" field');
            return false;
        }

        if( !in_array(count($this->input->post('teams[]')), array(8,16,32,64)) ) {
            $this->form_validation->set_message('rule_valid_team_selection', 'The {field} selected should be in 8,16,32,64 range');
            return false;
        }

        return true;
    }
}