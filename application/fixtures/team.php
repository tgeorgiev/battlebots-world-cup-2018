<?php

//source https://battlebots.com/2018-season-robots/
return array(
    ['id' => 1,  'name' => 'Axe Backwards'],
    ['id' => 2,  'name' => 'Bale Spear'],
    ['id' => 3,  'name' => 'Basilisk'],
    ['id' => 4,  'name' => 'Battle Royale With Cheese'],
    ['id' => 5,  'name' => 'Bite Force'],
    ['id' => 6,  'name' => 'Blacksmith'],
    ['id' => 7,  'name' => 'Bombshell'],
    ['id' => 8,  'name' => 'Bronco'],
    ['id' => 9,  'name' => 'Brutus'],
    ['id' => 10, 'name' => 'Captain Shrederator'],
    ['id' => 11, 'name' => 'Chomp'],
    ['id' => 12, 'name' => 'Deviled Egg'],
    ['id' => 13, 'name' => 'Double Dutch'],
    ['id' => 14, 'name' => 'Double Jeopardy'],
    ['id' => 15, 'name' => 'DUCK'],
    ['id' => 16, 'name' => 'End Game'],
);