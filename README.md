## Project Request
Develop a web-based software using CodeIgniter/Mysql which should include the following components:

- team management - add/delete (fields : id, name);
- tournament creation/management (fields: id, name, number of teams - 8/16/32/64);
- functionality to create tournament scheme- divide the teams from 1 to N in groups by two( example: champions league knockout phase)
- functionality to populate the results of each game and automatic creation of next match if the previous 2 winners are known;
- validation of the data entered by the user;

## Description
   The project is realised with CodeIgniter v.3.1.9.

![project screenshot][screenshot_01]

**Static analysis of the project**
![project screenshot][screenshot_02]

## Installation
1. Clone the project from the repo
    ```
    git clone https://tgeorgiev@bitbucket.org/tgeorgiev/battlebots-world-cup-2018.git
    ```
2. enter in the project `cd battlebots-world-cup-2018`
3. setup a local vhost `http://bwc.local/`
4. create DB `bwc_local` from `application/fixtures/db_schema.sql`
5. load fixtures from `http://bwc.local/fixtures`

## Notes
1. You can view a [online demo](http://battlebot.nutrapress.com) of the project
2. Or you can see it on [youtube](https://youtu.be/uTjr9KYvZrg)


[screenshot_01]: https://bitbucket.org/tgeorgiev/battlebots-world-cup-2018/raw/master/assets/img/screenshot_01.png "Screenshot"
[screenshot_02]: https://bitbucket.org/tgeorgiev/battlebots-world-cup-2018/raw/master/assets/img/screenshot_02.png "Screenshot"
